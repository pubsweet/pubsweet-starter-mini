const { pickBy } = require('lodash')

class AuthsomeMode {
  /**
   * Creates a new instance of AuthsomeMode
   *
   * @param {string} userId A user's UUID
   * @param {string} operation The operation you're authorizing for
   * @param {any} object The object of authorization
   * @param {any} context Context for authorization, e.g. database access
   * @returns {string}
   */
  constructor(userId, operation, object, context) {
    this.userId = userId
    this.operation = AuthsomeMode.mapOperation(operation)
    this.object = object
    this.context = context
  }

  /**
   * Maps operations from HTTP verbs to semantic verbs
   *
   * @param {any} operation
   * @returns {string}
   */
  static mapOperation(operation) {
    const operationMap = {
      GET: 'read',
      POST: 'create',
      PATCH: 'update',
      DELETE: 'delete',
    }

    return operationMap[operation] ? operationMap[operation] : operation
  }

  async isTeamMember(teamType, object) {
    let membershipCondition
    if (object) {
      membershipCondition = team =>
        team.teamType === teamType &&
        team.object &&
        team.object.id === object.id
    } else {
      membershipCondition = team => team.teamType === teamType
    }

    const memberships = await Promise.all(
      this.user.teams.map(async teamId => {
        const teamFound = await this.context.models.Team.find(teamId)
        if (teamFound) {
          return membershipCondition(teamFound)
        }
        return false
      }),
    )

    return memberships.includes(true)
  }

  isAuthor(object) {
    return this.isTeamMember('author', object)
  }

  async findCollectionByObject(object) {
    const { type, id, book, object: collection } = object
    let collectionId
    switch (type) {
      case 'fragment':
        collectionId = book
        break
      case 'team':
        collectionId = collection.id
        break
      default:
        collectionId = id
        break
    }
    if (id) {
      return this.context.models.Collection.find(collectionId)
    }
    return undefined
  }
  async canRead() {
    this.user = await this.context.models.User.find(this.userId)

    // const collection = await this.findCollectionByObject(this.object)

    // const permission = await this.isAuthor(collection)

    return true
  }

  async canListCollections() {
    this.user = await this.context.models.User.find(this.userId)

    return {
      filter: async collections => {
        const filteredCollections = await Promise.all(
          collections.map(async collection => {
            const condition = await this.isAuthor(collection)
            return condition ? collection : undefined
          }, this),
        )

        return filteredCollections.filter(collection => collection)
      },
    }
  }

  async canReadUser() {
    this.user = await this.context.models.User.find(this.userId)

    if (this.user.id === this.object.id) {
      return true
    }
    return {
      filter: user =>
        pickBy(user, (_, key) => ['id', 'username', 'type'].includes(key)),
    }
  }

  async canListTeams() {
    this.user = await this.context.models.User.find(this.userId)

    return {
      filter: async teams => {
        const filteredTeams = await Promise.all(
          teams.map(async team => {
            const condition = this.belongsToTeam(team.id)
            return condition ? team : undefined
          }, this),
        )

        return filteredTeams.filter(team => team)
      },
    }
  }

  belongsToTeam(teamId) {
    return this.user.teams.includes(teamId)
  }

  async canReadTeam() {
    this.user = await this.context.models.User.find(this.userId)
    return true
  }

  async canCreateTeam() {
    this.user = await this.context.models.User.find(this.userId)
    return true
  }

  async canUpdateTeam() {
    this.user = await this.context.models.User.find(this.userId)
    return true
  }

  async canCreateCollection() {
    this.user = await this.context.models.User.find(this.userId)
    return true
  }
}

module.exports = {
  before: async (userId, operation, object, context) => {
    const user = await context.models.User.find(userId)
    return user && user.admin
  },
  GET: (userId, operation, object, context) => {
    // const mode = new AuthsomeMode(userId, operation, object, context)

    // GET /api/collections
    if (object && object.path === '/collections') {
      return true
    }
    // GET /api/collection
    if (object && object.type === 'collection') {
      return true
    }

    // GET /api/collections/:collectionId/fragments
    if (object && object.path === '/fragments') {
      return true
    }
    // GET /api/collections/:collectionId/fragments/:fragmentId
    if (object && object.type === 'fragment') {
      return true
    }

    // GET /api/users
    if (object && object.path === '/users') {
      return true
    }

    // // GET /api/teams
    if (object && object.path === '/teams') {
      return true
    }

    // // GET /api/team
    if (object && object.type === 'team') {
      return true
    }

    // // GET /api/user
    if (object && object.type === 'user') {
      return true
    }

    return false
  },
  POST: (userId, operation, object, context) => {
    const mode = new AuthsomeMode(userId, operation, object, context)
    // POST /api/collections
    if (object && object.path === '/collections') {
      return mode.canCreateCollection()
    }
    // POST /api/users
    if (object && object.path === '/users') {
      return true
    }
    // POST /api/fragments
    if (object && object.path === '/collections/:collectionId/fragments') {
      return true
    }
    // POST /api/teams
    if (object && object.path === '/teams') {
      return true
    }

    return false
  },
  PATCH: (userId, operation, object, context) => {
    // const mode = new AuthsomeMode(userId, operation, object, context)
    // PATCH /api/collections/:id
    let data
    if (object) {
      if (object.current) {
        data = object.current
      } else {
        data = object
      }
    } else {
      return false
    }

    if (data.type === 'collection') {
      return true
    }
    // PATCH /api/fragments/:id
    if (data.type === 'fragment') {
      return true
    }
    // PATCH /api/teams/:id
    if (data.current.type === 'team') {
      return true
    }

    return false
  },
  DELETE: (userId, operation, object, context) => {
    // const mode = new AuthsomeMode(userId, operation, object, context)
    // DELETE /api/collections/:id
    if (object && object.type === 'collection') {
      return true
    }
    // DELETE /api/fragments/:id
    if (object && object.type === 'fragment') {
      return true
    }

    // DELETE /api/teams/:id
    if (object && object.type === 'team') {
      return true
    }

    return false
  },
}
