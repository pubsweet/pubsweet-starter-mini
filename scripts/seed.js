#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const { Collection, Fragment, User } = require('pubsweet-server/src/models')
const { setupDb } = require('@pubsweet/db-manager')

const seed = async () => {
  await setupDb({
    username: 'admin',
    password: 'password',
    email: 'admin@example.com',
    admin: true,
    clobber: true,
  })

  const user = await new User({
    username: 'john',
    email: 'john@example.com',
    password: 'johnjohn',
  }).save()

  const collection = new Collection({
    title: 'My Blog',
    owners: [user.id],
  })
  await collection.save()

  const fragment1 = await new Fragment({
    title: 'A great paper',
    authors: ['Yannis Barlas', 'Adam Hyde'],
    owners: [user.id],
  }).save()

  const fragment2 = await new Fragment({
    title: 'A magnificent paper',
    authors: ['Alex Theg', 'Ana Ellis'],
    owners: [user.id],
  }).save()

  const fragment3 = await new Fragment({
    title: 'A superb paper',
    authors: ['Charlie Rutter', 'Carly Strauss'],
    owners: [user.id],
  }).save()

  const fragment4 = await new Fragment({
    title: 'A fantastic paper',
    authors: ['Alisson Zulowski', 'John Chodaki'],
    owners: [user.id],
  }).save()

  // const fragment2 = await new Fragment({
  //   title: 'My second post',
  //   owners: [user.id],
  // }).save()

  collection.addFragment(fragment1)
  collection.addFragment(fragment2)
  collection.addFragment(fragment3)
  collection.addFragment(fragment4)
  await collection.save()

  logger.info('Seeding complete.')
}

seed()
