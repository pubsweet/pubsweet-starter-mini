import React from 'react'
import PropTypes from 'prop-types'

import Navigation from './Navigation/Navigation'

const App = ({ children, ...props }) => (
  <div>
    <Navigation />
    {children}
  </div>
)

App.propTypes = {
  children: PropTypes.node.isRequired,
}

export default App
