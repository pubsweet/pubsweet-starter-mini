import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Authorize from 'pubsweet-client/src/helpers/Authorize'
import actions from 'pubsweet-client/src/actions'
import { Action, AppBar } from '@pubsweet/ui'

const Navigation = ({ logoutUser, currentUser }) => (
  <AppBar
    brand={<img alt="pubsweet" src="/assets/pubsweet.jpg" />}
    navLinkComponents={[
      <Action to="/manage/posts">Posts</Action>,
      <Authorize object={{ path: '/users' }} operation="GET">
        <Action to="/manage/users">Users</Action>
      </Authorize>,
    ]}
    onLogoutClick={logoutUser}
    user={currentUser}
  />
)

Navigation.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  // eslint-disable-next-line react/require-default-props
  currentUser: PropTypes.shape({
    username: PropTypes.string,
    admin: PropTypes.bool,
  }),
}

export default connect(
  state => ({
    currentUser: state.currentUser.user,
  }),
  { logoutUser: actions.logoutUser },
)(Navigation)
