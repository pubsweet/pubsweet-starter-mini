import React from 'react'
import { Route, Switch } from 'react-router-dom'

// Manage
import PostsManager from 'pubsweet-component-posts-manager/PostsManagerContainer'
import UsersManager from 'pubsweet-component-users-manager/src/UsersManagerContainer'

// Public
import Blog from 'pubsweet-component-blog/BlogContainer'
import HTML from 'pubsweet-component-html/HTMLContainer'

// Authentication
import Login from 'pubsweet-component-login/LoginContainer'
import Signup from 'pubsweet-component-signup/SignupContainer'
import PasswordReset from 'pubsweet-component-password-reset-frontend/PasswordReset'
import AuthenticatedComponent from 'pubsweet-client/src/components/AuthenticatedComponent'

import App from './components/App'

// eslint-disable-next-line
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthenticatedComponent>
        <Component {...props} />
      </AuthenticatedComponent>
    )}
  />
)

export default (
  <App>
    <Switch>
      <PrivateRoute component={UsersManager} path="/manage/users" />
      <PrivateRoute component={PostsManager} path="/manage/posts" />

      <Route component={Blog} exact path="/" />
      <Route component={Login} path="/login" />
      <Route component={Signup} path="/signup" />
      <Route component={PasswordReset} path="/password-reset" />
      <Route component={HTML} path="/:id" />
    </Switch>
  </App>
)
